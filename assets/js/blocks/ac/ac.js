define(function(require) {
	var Marionette = require('marionette'),
		Howl = require('howler').Howl,
		AcExtraControls = require('block!ac/__ectrls'),
		formDuration = require('helpers/formDuration');
	require('jquery.linemover');
    require('jquery.cookie');

	var AcView = Marionette.View.extend({
		isPlaying: false,
		collection: null,
		playPositionPercent: 0,
		volume: 0,
		starttVolume: 90,
		updatePlaypositionInterval: 100,
        connect: null,
		pauseClassName: 'ac__play_pause',
        volumeCookieName: 'volume',
		el: '.ac',
		ui: {
			'play': '.ac__play',
			'vol': '.ac__vol',
			'pr': '.ac__pr',
			'performer': '.ac__performer',
			'title': '.ac__title',
			'duration': '.ac__duration'
		},
		events: {
			'click .ac__play': 'playToggle',
			'click .ac__next': 'next',
			'click .ac__prev': 'prev'
		},
		initialize: function(options) {
			var that = this;
			this.options = options || {};
			this.extraControls = new AcExtraControls({
				collection: this.collection,
                devicesCollection: this.options.devicesCollection,
				ac: this
			});
			this.bindUIElements();
			this.audio = new Howl({});
			//this.setCollection(this.options.collection);

			setInterval(function() {
				that.updatePlayposition.apply(that);
			}, this.updatePlaypositionInterval);

			this.setVolume(this.getDefaultVolume());

			this.collection.on('change:position', function(pos) {
				that.updateSong.apply(that);
			});
			this.collection.on('play', this.play, this);
			this.collection.on('pause', this.pause, this);

            this.on('change:volume', this.writeVolumeToCookie, this);
		},
		// setCollection: function(collection) {
		// 	if(_.isEmpty(collection)) {
		// 		return;
		// 	}

		// 	var urls = collection.map(function(model) {
		// 		return model.get('url');
		// 	});
		// 	this.audio.urls(urls);
		// },
		updateSong: function() {
			var that = this,
				model = this.collection.getActiveModel();
			this.ui.performer.text(model.get('artist'));
			this.ui.title.text(model.get('title'));
			this.ui.duration.text(formDuration(model.get('duration')));
			//this.audio.stop();
			this.audio.unload();
			this.audio = new Howl({
				volume: this.getVolume() / 100
			});
			that.audio.urls([model.get('url')]);
			//this.audio.pos(this.collection.getActivePosition(), 0);
			//console.log(this.collection.getActivePosition());
		},
		render: function() {
			var that = this;

			this.ui.vol.find('.ac__progress-line').linemover({
                value: this.getVolume(),
				onChange: function(val) {
					that.setVolume(val);
					that.trigger('volume:change', val);
				}
			});
			this.ui.pr.find('.ac__progress-line').linemover({
				onChange: function(val) {
					that.playPositionPercent = val;
				},
				onEnd: function(val) {
					that.setPlayPosition(val);
					that.trigger('playposition:change', val);
				}
			});

			this.extraControls.render();
		},
		playToggle: function() {
            this.collection.toggle();
		},
		play: function() {
			if(this.isPlaying) {
				return;
			} else {
				this.isPlaying = true;
			}

			//var model = this.collection.getActiveModel();
			//model.play();

			this.audio.play();
			this.ui.play.addClass(this.pauseClassName);
			console.log('play');

            this.blockWindowUnload();
		},
		pause: function() {
			if(!this.isPlaying) {
				return;
			} else {
				this.isPlaying = false;
			}

			//var model = this.collection.getActiveModel();
			//model.pause();

			this.audio.pause();
			this.ui.play.removeClass(this.pauseClassName);
			console.log('pause');

            this.allowWindowUnload();
		},
		prev: function() {
			var model = this.collection.getPrevModel();
			this.collection.toggleModel(model);

			this.trigger('prev');
		},
		next: function() {
			var model = this.collection.getNextModel();
			this.collection.toggleModel(model);

			this.trigger('next');
		},
		setPlayPosition: function(val) {
			this.playposition = val;
			var d = this.audio._duration * val / 100;
			this.audio.pos(d);
			this.trigger('change:playposition', val);
		},
		setVolume: function(val) {
			this.volume = val;
			this.audio.volume(val/100);
			this.trigger('change:volume', val);
		},
		getPlayPosition: function() {
			return this.playPositionPercent;
		},
		getVolume: function() {
			return this.volume;
		},
		updatePlayposition: function() {
			var model = this.collection.getActiveModel();

			if(_.isUndefined(model)) {
				return;
			}

			var perc = 100 * this.audio.pos() / model.get('duration');
			this.ui.pr.find('.ac__progress-line').css({
				width: perc + '%'
			});

			if(100 <= perc) {
				this.next();
			}
		},
        writeVolumeToCookie: function() {
            var volume = this.getVolume();
            $.cookie(this.volumeCookieName, volume);
        },
        getDefaultVolume: function() {
            var volume = $.cookie(this.volumeCookieName);

            if(!volume) {
                volume = this.starttVolume;
            }

            return volume;
        },
        setConnect: function(connect) {
            this.connect = connect;
            this.trigger('change:connect', connect);
        },
        getConnect: function() {
            return this.connect;
        },
        allowWindowUnload: function() {
            $(window).unbind('beforeunload');
        },
        blockWindowUnload: function() {
            $(window).bind('beforeunload', function(event) {
                event.stopPropagation();
                event.returnValue = 'Вы уверены, что хотите закрыть окно?';
                return event.returnValue;
            });
        }
	});

	return AcView;
})