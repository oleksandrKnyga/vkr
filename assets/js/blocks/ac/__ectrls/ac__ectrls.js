define(function(require) {
	var Marionette = require('marionette'),
		ContextDevicesFormView = require('block!cntf/_devices');

	var AcExtraControls = Marionette.View.extend({
		el: '.ac__ectrls',
		initialize: function(options) {
			this.options = options || {};
			this.statusForm = new ContextDevicesFormView({
				elCaller: this.$el.find('.ac__status'),
                songsCollection: options.collection,
                collection: options.devicesCollection,
                ac: options.ac
			});
		},
		events: {
			'click .ac__status': 'actionStatus'
		},
		actionStatus: function(e) {
			this.statusForm.toggle();
		}
	});

	return AcExtraControls;
}) 