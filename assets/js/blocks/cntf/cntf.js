define(function(require) {
	var Marionette = require('marionette'),
		$ = require('jquery'),
		ContextFormTemplate = require('textblock!cntf.html');
    require('jquery.cookie');

	var ContextFormView = Marionette.View.extend({
		isShowed: false,
		isRendered: false,
		shifts: {
			top: 3,
			left: 13.5
		},
		ui: {
			title: '.cntf__title',
			description: '.cntf__description',
			extras: '.cntf__exras'
		},
		initialize: function(options) {
			this.options = _.extend({}, {
				data: {}
			}, options);
		},
		render: function() {
			this.trigger('render:before');
			var html = _.template(ContextFormTemplate, this.options.data);
			var $html = $(html);
			
			this.setElement($html);
			$('body').append($html);

			if(this.options.width) {
				this.$el.css({
					width: this.options.width
				});
			}

			if(this.options.className) {
				this.$el.addClass(this.options.className);
			}

			if(this.options.elCaller) {
				var $elCaller = $(this.options.elCaller);
				var offset = $elCaller.offset();
				this.$el.css({
					top: offset.top + 2*$elCaller.height() + this.shifts.top,
					left: offset.left - this.$el.width() + $elCaller.width() + this.shifts.left
				});
			}

			this.bindUIElements();
			this.delegateEvents();
			this.isShowed = true;
			this.isRendered = true;
			this.trigger('render:after');
		},
		show: function() {
			this.trigger('show:before');
			this.isShowed = true;
			this.$el.show();
			this.trigger('show:after');
		},
		hide: function() {
			this.trigger('hide:before');
			this.isShowed = false;
			this.$el.hide();
			this.trigger('hide:after');
		},
		toggle: function() {
			if(this.isRendered) {

				if(this.isShowed) {
					this.hide();
				} else {
					this.show();
				}
			} else {
				this.render();
			}
		}
	});

	return ContextFormView;
})