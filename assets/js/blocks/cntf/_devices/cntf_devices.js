define(function(require) {
    var Marionette = require('marionette'),
        $ = require('jquery'),
        ContextFormView = require('block!cntf'),
        ContextChecksView = require('block!cntf/__checks'),
        UserModel = require('models/user');

    var ContextDevicesView = ContextFormView.extend({
        lastFetchData: {},
        initialize: function(options) {
            var that = this;
            var data = {
                title: 'Трансляция аудиозаписей',
                description: 'Выберите устройство, на которое хотите транслировать проигрываемые аудиозаписи.',
                extras: '<div class="cntf__checks"></div>'
            };

            if(!_.isObject(options)) {
                options = {};
            }

            options = _.extend({}, {
                data: data,
                width: 260,
                className: 'cntf_devices'
            }, options);
            this.options = options;

            this.user = UserModel.getCurrent();

            this.on('render:after', function() {
                this.checks = new ContextChecksView({
                    el: this.$el.find('.cntf__checks'),
                    collection: this.collection
                });
                this.updateDevices();
            }, this);

            this.on('show:after', function() {
                this.updateDevices();
            }, this);

            this.collection.on('change:active', function(model) {
                console.log('change active');
                this.activeModel = model;
                this.sendAction('fetch', this.lastFetchData);
            }, this);

            //external bindings
            this.options.songsCollection.on('reset', function(collection, options) {
                this.lastFetchData = options;
                this.sendAction('fetch', options);
            }, this);
            this.options.songsCollection.on('change:position', function(position) {
                this.sendAction('change-position', position);
            }, this);
            this.options.songsCollection.on('play', function() {
                this.sendAction('play');
            }, this);
            this.options.songsCollection.on('pause', function() {
                this.sendAction('pause');
            }, this);
            this.options.ac.on('change:playposition', function(position) {
                this.sendAction('playposition', position);
            }, this);
            this.options.ac.on('change:volume', function(volume) {
                this.sendAction('volume', volume);
            }, this);

            ContextFormView.prototype.initialize.call(this, options);
        },
        sendAction: function(action, message) {
            var activeDevice = this.activeModel;

            if(!_.isEmpty(activeDevice)) {
                var data = {
                    action: action,
                    user_id: this.user.getId(),
                    device_id: activeDevice.getId(),
                    hash: location.hash,
                    data: message
                };

                this.options.ac.connect.emit('message', data);
                console.log(data);
            }
        },
        updateDevices: function() {
            var that = this;
            this.collection.fetch({
                reset: true,
                data: {
                    user_id: UserModel.getCurrent().getId()
                },
                success: function(collection) {
                    that.checks.render();
                    var model = collection.find(function(ml){
                        return !_.isUndefined(that.activeModel) &&
                            ml.getHash() == that.activeModel.getHash();
                    });

                    if(!_.isEmpty(model)) {
                        model.set('isActive', true);
                    }
                }
            });
        }
    });

    return ContextDevicesView;
})