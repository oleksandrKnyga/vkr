define(function(require) {
	var Marionette = require('marionette'),
		ControlFormEmptyTemplate = require('textblock!cntf/__checks/_empty.html');

	var ControlFormEmptyView = Marionette.View.extend({
		render: function() {
			var html = _.template(ControlFormEmptyTemplate, {});
			this.$el.html(html);
		}
	});

	return ControlFormEmptyView;
})