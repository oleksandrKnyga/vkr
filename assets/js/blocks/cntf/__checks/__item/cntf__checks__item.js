define(function(require) {
	var Marionette = require('marionette'),
		$ = require('jquery'),
		ControlFormChecksItemTemplate = require('textblock!cntf/__checks/__item.html');

	var ControlFormChecksItemView = Marionette.ItemView.extend({
		tagName: 'div',
		className: 'cntf__checks__item',
		activeClassName: 'cntf__checks__item_active',
		template: _.template(ControlFormChecksItemTemplate),
		events: {
			'click': 'click'
		},
		initialize: function(options) {
			this.parent = options.parent;
			this.model = options.model;
			this.model.set('isActive', false);

            this.model.on('change', function() {
                if(this.model.get('isActive')) {
                    this.activate();
                } else {
                    this.deactivate();
                }
            }, this);
		},
		click: function() {
            var isActive = this.model.get('isActive');
			this.parent.deactivateAll();

            if(!isActive) {
                this.activate();
            }
		},
		activate: function() {
			this.$el.addClass(this.activeClassName);
			this.model.set('isActive', true);
//			this.parent.activeView = this;
//			this.parent.activeModel = this.model;
		},
		deactivate: function() {
			this.$el.removeClass(this.activeClassName)
			this.model.set('isActive', false);
		}
	});

	return ControlFormChecksItemView;
})