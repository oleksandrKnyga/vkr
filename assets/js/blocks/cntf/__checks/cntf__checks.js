define(function(require) {
	var Backbone = require('backbone'),
		Marionette = require('marionette'),
		$ = require('jquery'),
		ControlFormChecksItemView = require('block!cntf/__checks/__item'),
		ControlFormChecksEmptyView = require('block!cntf/__checks/_empty'),
        UserModel = require('models/user');

	var ControlFormChecksView = Marionette.CollectionView.extend({
		childView: ControlFormChecksItemView,
		emptyView: ControlFormChecksEmptyView,
		initialize: function(options) {
			this.childViewOptions = {
				parent: this
			};

            if(_.isUndefined(options.collection)) {
                this.collection = new Backbone.Collection;
            } else {
                this.collection = options.collection;
            }

            this.collection.on('reset', this.render, this);
		},
		deactivateAll: function() {
			this.children.each(function(view) {
				view.deactivate();
			});
		}
	});

	return ControlFormChecksView;
})