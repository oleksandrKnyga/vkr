define(function(require) {
	var Marionette = require('marionette');

	var AudioSideView = Marionette.View.extend({
		el: '.audio__side',
		selectedClassName: 'album__item_selected',
		getElement: function(className) {
			return this.$el.find('.'+className);
		},
		unselectAll: function() {
			var that = this;
			this.$el.find('.'+this.selectedClassName).each(function() {
				$(this).removeClass(that.selectedClassName);
			});
		},
		deselectElement: function(className) {
			this.getElement(className).removeClass(this.selectedClassName);
		},
		selectElement: function(className) {
			this.unselectAll();
			this.getElement(className).addClass(this.selectedClassName);
		},
		hideElement: function(className) {
			this.getElement(className).hide();
		},
		showElement: function(className) {
			this.getElement(className).show();
		}
	});

	return AudioSideView;
})