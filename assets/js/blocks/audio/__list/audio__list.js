define(function(require) {
	var Marionette = require('marionette'),
		SongsCollection = require('collections/songs'),
		AudioListItemView = require('block!audio/__list/__item'),
		AudioListEmptyView = require('block!audio/__list/_loading');

	var AudioListView = Marionette.CollectionView.extend({
		el: '.audio__list',
		pauseClassName: 'audio__item__play-btn_pause',
		childView: AudioListItemView,
		emptyView: AudioListEmptyView,
		initialize: function(options) {
			this.options = options;
			this.childViewOptions = {
				parent: this,
				user: this.options.user
			};
		},
		getView: function(model) {
			return this.children._views[this.children._indexByModel[model.cid]];
		},
		getActiveView: function() {
			return this.getView(this.collection.getActiveModel());
		},
		toggle: function(model) {
			this.collection.toggleModel(model);
		}
	});

	return AudioListView;
})