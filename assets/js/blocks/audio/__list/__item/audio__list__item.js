define(function(require) {
	var Marionette = require('marionette'),
		AudioListItemTemplate = require('textblock!audio/__list/__item.html'),
		formDuration = require('helpers/formDuration');

	var AudioListItemView = Marionette.ItemView.extend({
		template: _.template(AudioListItemTemplate),
		className: 'audio__list__item fl-l',
		tagName: 'div',
		ui: {
			'play': '.audio__item__play-btn'
		},
		templateHelpers: {
			formDuration: formDuration
		},
		events: {
			'click .audio__item__play-btn': 'toggle',
			'click .audio__item__action_remove-audio': 'removeFromOwnCollection',
			'click .audio__item__action_add-audio': 'addToOwnCollection'
		},
		initialize: function(options) {
			var that = this;
			this.parent = options.parent;
			this.model = options.model;
            this.user = options.user;
			this.templateHelpers.user = options.user;

			this.model.on('play', function() {
                console.log('xxx: play');
                this.bindUIElements();
				this.ui.play.addClass(this.parent.pauseClassName);
			}, this);
			this.model.on('pause', function() {
                console.log('xxx: pause');
                this.bindUIElements();
				this.ui.play.removeClass(this.parent.pauseClassName);
			}, this);
		},
        render: function() {
            Marionette.ItemView.prototype.render.apply(this, arguments);
        },
		toggle: function() {
            console.log('toggle');
			this.parent.toggle(this.model);
		},
		getPosition: function() {
			return this.$el.index();
		},
		removeFromOwnCollection: function() {
			this.model.removeFromOwnCollection(this.user.getId());
			this.model.collection.remove(this.model);
		},
		addToOwnCollection: function() {
			this.model.addToOwnCollection(this.user.getId());
		}
	});

	return AudioListItemView;
})