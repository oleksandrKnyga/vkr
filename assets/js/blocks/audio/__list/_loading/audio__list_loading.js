define(function(require) {
	var Marionette = require('marionette'),
		AudioListLoadingTemplate = require('textblock!audio/__list/_loading.html');

	var AudioListLoadingView = Marionette.View.extend({
		render: function() {
			var html = _.template(AudioListLoadingTemplate, {});
			this.$el.html(html);
		}
	});

	return AudioListLoadingView;
})