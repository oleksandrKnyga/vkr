define(function(require) {
	var Marionette = require('marionette'),
		AudioListEmptyTemplate = require('textblock!audio/__list/_empty.html');

	var AudioListEmptyView = Marionette.View.extend({
		render: function() {
			var html = _.template(AudioListEmptyTemplate, {});
			this.$el.html(html);
		}
	});

	return AudioListEmptyView;
})