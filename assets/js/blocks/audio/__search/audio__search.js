define(function(require) {
	var Marionette = require('marionette');

	var AudioSearchView = Marionette.View.extend({
		el: '.audio__search',
		pause: 250,
		timeout: 0,
		ui: {
			'field': '.audio__search__f',
			'reset': '.search-reset',
			'progress': '.search-progress'
		},
		events: {
			'keyup .audio__search__f': 'keyUpSearch',
            'click .button_flat': 'search',
			'click .search-reset': 'clear'

		},
		render: function() {
			this.delegateEvents();
			this.bindUIElements();
		},
		setLoading: function(isLoading) {
			if(isLoading) {
				this.ui.reset.hide();
				this.ui.progress.show();
			} else {
				this.ui.reset.show();
				this.ui.progress.hide();
			}
		},
		keyUpSearch: function() {
			clearTimeout(this.timeout);
			this.timeout = setTimeout(_.bind(function() {
                this.search();
            }, this), this.pause);
			this.updateReset();
		},
        search: function() {
            val = this.ui.field.val();
            this.trigger('search', val);
        },
		updateReset: function() {
			var val = this.ui.field.val();

			if(val.length > 0) {
				this.ui.reset.show();
			} else {
				this.ui.reset.hide()
			}
		},
		setValue: function(query) {
			this.ui.field.val(query);
			this.updateReset();
		},
		clear: function(isUpdateUrl) {
            if(_.isUndefined(isUpdateUrl)) {
                isUpdateUrl = false;
            }

			var val = '';
			this.ui.field.val(val);
			this.updateReset();

            if(isUpdateUrl) {
                this.trigger('search', val);
            }
		}
	});

	return AudioSearchView;
});