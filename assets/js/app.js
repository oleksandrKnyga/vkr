require.config({
	baseUrl: '/js',
	paths: {
        'jshash': '../components/jshash',
		'text': '../components/requirejs.text/text',
		'block': '../components/requirejs.block/block',
		'textblock': '../components/requirejs.block/textblock',
		'jquery': '../components/jquery/dist/jquery.min',
		'backbone': '../components/backbone/backbone',
		'underscore': '../components/underscore/underscore',
		'marionette': '../components/marionette/lib/backbone.marionette.min',
		'jquery.cookie': '../components/jquery.cookie/jquery.cookie',
		'jquery.linemover': '../components/jquery.linemover/jquery.linemover',
		'howler': '../components/howler.js/howler',
        'wichbrowser': '../components/WhichBrowserJS/whichbrowser-js-min',
        'sails.io': '../components/sails.io/sails.io',
        'vkapi': '../components/vkapi/vkapi'
	},
	shim: {
		'underscore': {
			exports: '_'
		},
		'backbone': {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		'marionette': {
			'deps': ['backbone'],
			exports: 'Marionette'
		},
		'jquery.cookie': {
			deps: ['jquery']
		},
		'jquery.linemover': {
			deps: ['jquery']	
		}
	}
});

require(['backbone', 'marionette', 'controllers/_'], function(Backbone, Marionette, Controllers) {
	var app = new Marionette.Application();

	app.addInitializer(function(options) {
		new (Backbone.Marionette.AppRouter.extend({
			controller: new Controllers.Site(),
			appRoutes: {
				'': 'actionIndex',
				'search': 'actionSearch',
                'popular': 'actionPopular',
                'recomendations': 'actionRecomendations',
                'popular-eng': 'actionPopularEnglish'
			}
		}))();
	});

	app.on('start', function(options) {
		if (Backbone.history) {
			Backbone.history.start();
		}
	});

	app.start();
});