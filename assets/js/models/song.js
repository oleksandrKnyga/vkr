define(function(require) {
    var Backbone = require('backbone'),
        vkapi = require('vkapi');

    var SongModel = Backbone.Model.extend({
        url: '/api/song',
        idAttribute: 'id',
        defaults: {
            id: 0,
            owner_id: 0,
            artist: '',
            title: '',
            duration: 0,
            url: '',
            lyrics_id: 0,
            album_id: 0,
            genre_id: 0
        },
        pause: function() {
            this.trigger('pause');
        },
        play: function() {
            this.trigger('play');
        },
        getId: function() {
            return this.get(this.idAttribute);
        },
        removeFromOwnCollection: function(ownerId) {
            var options = {
                reset: false,
                action: 'audio.delete',
                data: {
                    audio_id: this.getId(),
                    owner_id: ownerId
                },
                callback: function(response) {
                    console.log(response);
                }
            };

            vkapi.exec(options, this);
        },
        addToOwnCollection: function(ownerId) {
            var options = {
                reset: false,
                action: 'audio.add',
                data: {
                    audio_id: this.getId(),
                    owner_id: ownerId
                },
                callback: function(response) {
                    console.log(response);
                }
            };

            vkapi.exec(options, this);
        }
    });

    return SongModel;
});