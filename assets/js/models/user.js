define(function(require) {
    var Backbone = require('backbone'),
        sha1 = require('jshash/sha1');
    require('jquery.cookie');

    var UserModel = Backbone.Model.extend({
        url: '/api/user',
        idAttribute: 'id',
        defaults: {
            id: 0
        },
        getId: function() {
            return this.get(this.idAttribute);
        }
    }, {
        getCurrent: function() {
            return new UserModel(JSON.parse($.cookie('user')));
        }
    });

    return UserModel;
});