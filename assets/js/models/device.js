define(function(require) {
	var Backbone = require('backbone'),
        hashJava32bit = require('jshash/java32bit'),
        sha1 = require('jshash/sha1');
    require('wichbrowser');

	var DeviceModel = Backbone.Model.extend({
		defaults: {
			id: 0,
			isActive: false,
			name: ''
		},
        initialize: function() {
            this.on('change:isActive', function() {
                this.collection.trigger('change:active', this);
            }, this);
        },
        getId: function() {
            var id = this.get('id');

            if(!_.isEmpty(id)) {
                return id;
            } else {
                return this.getHash();
            }
        },
        getName: function() {
            var nameAttributes = [['os_name', 'os_version'], ['browser_name', 'browser_version'], 'ip'],
                nameValues = [],
                maxLength = 38;

            _.each(nameAttributes, function(name) {
                if(_.isString(name)) {
                    var value = this.get(name);

                    if(!_.isUndefined(value)) {
                        nameValues.push(value);
                    }
                } else {
                    var lnv = [];
                    _.each(name, function(name) {
                        var value = this.get(name);

                        if(!_.isUndefined(value)) {
                            lnv.push(value);
                        }
                    }, this);

                    if(lnv.length > 0) {
                        nameValues.push(lnv.join(' '));
                    }
                }

            }, this);

            var name = nameValues.join(', ');

            if(name.length > maxLength) {
                name = name.substr(0, maxLength) + '..';
            }

            return name;
        },
        getHash: function() {
            var fdata = {},
                allowed = ['os_name', 'os_version', 'browser_name',
                    'browser_version', 'device_type', 'device_manufacturer',
                    'device_model', 'user_uid', 'ip'],
                data = this.attributes;

            for(var name in data) {

                if(allowed.indexOf(name) > -1) {
                    fdata[name] = data[name];
                }
            }

//            console.log(fdata);
//            console.log(sha1(JSON.stringify(fdata)));

            return sha1(JSON.stringify(fdata));

            //var hash = crypto.createHash('md5').update(JSON.stringify(fdata)).digest('hex');
            //return hash;
        },
        setParamsFromWhichBrowser: function(data) {
            var params = {};

            if(data.os && data.os.name) {
                params.os_name = data.os.name;
            }

            if(data.os && data.os.version) {
                params.os_version = data.os.version.alias ? data.os.version.alias : data.os.version.value;
            }

            if(data.browser && data.browser.name) {
                params.browser_name = data.browser.name;
            }

            if(data.browser && data.browser.version) {
                params.browser_version = data.browser.version.alias ? data.browser.version.alias : data.browser.version.value;
            }

            if(data.device && data.device.type) {
                params.device_type = data.device.type;
            }

            if(data.device && data.device.manufacturer) {
                params.device_manufacturer = data.device.manufacturer;
            }

            if(data.device && data.device.model) {
                params.device_model = data.device.model;
            }

            this.set(params);
        }
	}, {
        getCurrentDevice: function(cb) {
            if(!_.isFunction(cb)) {
                cb = $.noop;
            }

            var whichBrowser = new WhichBrowser();
            whichBrowser.onReady(function(data) {
                var device = new DeviceModel();
                device.setParamsFromWhichBrowser(data);
                cb.call(device, device);
            });
        }
    });

	return DeviceModel;
})