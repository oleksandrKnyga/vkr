define(function(require) {
	var addLeadingZero = function(num, isLeadingZero) {
		if(num < 10 && isLeadingZero) {
			return '0' + num;
		} else {
			return num;
		}
	}

	var formDuration = function(totalSeconds, isLeadingZero) {
		if("undefined" === typeof isLeadingZero) {
			isLeadingZero = false;
		}

		var hours = parseInt(totalSeconds / 3600) % 24,
			minutes = parseInt(totalSeconds / 60) % 60,
			seconds = totalSeconds % 60;

		if(hours > 0) {
			return addLeadingZero(hours, isLeadingZero) + ':' + 
				addLeadingZero(minutes, isLeadingZero) + ':' + 
				addLeadingZero(seconds);
		} else {
			return addLeadingZero(minutes, isLeadingZero) + ':' + 
				addLeadingZero(seconds);
		}
	}

	return formDuration;
});