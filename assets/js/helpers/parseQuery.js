define(function(require) {
	var parseQuery = function(queryString) {
		var spt = queryString.split('&'),
			params = {};

		for(var i=0;i<spt.length;i++) {
			var pts = spt[i].split('=');
			params[pts[0]] = decodeURIComponent(pts[1]);
		}

		return params;
	};

	return parseQuery;
})