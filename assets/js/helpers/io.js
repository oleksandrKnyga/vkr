define(function(require) {
    var io = require('sails.io');

    var init = function() {
        var connect = io.connect('/');
        connect.emit('subscribe', {
            user_id: this.user.getId()
        });
        connect.on('message', _.bind(function(data) {

        }));
    }
})