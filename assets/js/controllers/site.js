define(function(require) {
	var $ = require('jquery'),
		Marionette = require('marionette'),
		AcView = require('block!ac'),
		AudioSearchView = require('block!audio/__search'),
		AudioSideView = require('block!audio/__side'),
		AudioListView = require('block!audio/__list'),
		SongsCollection = require('collections/songs'),
		parseQuery = require('helpers/parseQuery'),
        UserModel = require('models/user'),
        DeviceModel = require('models/device'),
        DevicesCollection = require('collections/devices'),
        io = require('sails.io'),
        sha1 = require('jshash/sha1'),
        vkapi = require('vkapi');
    require('jquery.cookie');

	var SiteController = Marionette.Controller.extend({
		initialize: function() {
			var that = this;

			this.songsCollection = new SongsCollection;
            this.devicesCollection = new DevicesCollection;
            this.user = UserModel.getCurrent();
            DeviceModel.getCurrentDevice(_.bind(function(device) { 
                device.set('ip', $.cookie('ip'));
                this.device = device;
                this.devicesCollection.setCurrentDevice(this.device);
                this.connectIO();
            }, this));

            var isFirstFetch = true;
            this.songsCollection.on('reset', function(collection) {
                if(isFirstFetch) {
                    console.log('change active position');
                    collection.setActivePosition(0);
                    isFirstFetch = false;
                } else {
                    collection.setActivePosition(-1, false);
                }
            });
            this.songsCollection.on('error', function(error) {
                location.href = '/site/logout';
            });
            this.songsCollection.setToken(this.user.get('token'));
            vkapi.set('token', this.user.get('token'));

			this.ac = new AcView({
				collection: this.songsCollection,
                devicesCollection: this.devicesCollection
			});
			this.ac.render();

			this.audioSearch = new AudioSearchView();
			this.audioSearch.on('search', function(q) {
				if(q && q.length > 0) {
					location.hash = '#/search?q='+q;
				} else {
					location.hash = '#';
				}
			});
			this.audioSearch.render();

			this.audioSide = new AudioSideView();

			this.audioList = new AudioListView({
				collection: this.songsCollection,
                user: this.user
			});
			this.audioList.render();

            this.ac.setConnect(this.connect);
		},
        connectIO: function() {
            var that = this;
            var connect = io.connect('/');
            connect.emit('subscribe', {
                user_id: this.user.getId(),
                device_id: this.device.getHash(),
                device_name: this.device.getName()
            });
            connect.on('message', function(data) {
                if(data.device_id == that.device.getHash()) {

                    location.hash = data.hash;

                    switch(data.action) {
                        case 'fetch':
                            //that.songsCollection.fetch(data.data);
                            break;
                        case 'change-position':
                            that.songsCollection.setActivePosition(data.data);
                            break;
                        case 'play':
                            that.songsCollection.play();
                            break;
                        case 'pause':
                            that.songsCollection.pause();
                            break;
                        case 'playposition':
                            that.ac.setPlayPosition(data.data);
                            break;
                        case 'volume':
                            that.ac.setVolume(data.data);
                            break;
                    }
                }
            });

            this.connect = connect;
        },
		actionIndex: function() {
			this.audioSearch.clear(true);
			this.audioSide.hideElement('album__item_search');
			this.audioSide.selectElement('album__item_my');
            this.songsCollection.fetch({
                reset: true
            });
		},
		actionSearch: function(query) {
			var params = parseQuery(query);
			this.audioSide.showElement('album__item_search');
			this.audioSide.selectElement('album__item_search');
			this.audioSearch.setValue(params.q);
            this.songsCollection.fetch({
                reset: true,
                action: 'audio.search',
                data: {
                    q: params.q
                }
            });
		},
        actionPopular: function(){
            this.audioSearch.clear();
            this.audioSide.hideElement('album__item_search');
            this.audioSide.selectElement('album__item_popular');
            this.songsCollection.fetch({
                reset: true,
                action: 'audio.getPopular'
            });
        },
        actionPopularEnglish: function(){
            this.audioSearch.clear();
            this.audioSide.hideElement('album__item_search');
            this.audioSide.selectElement('album__item_popular-eng');
            this.songsCollection.fetch({
                reset: true,
                action: 'audio.getPopular',
                data: {
                    only_eng: 1
                }
            });
        },
        actionRecomendations: function() {
            this.audioSearch.clear();
            this.audioSide.hideElement('album__item_search');
            this.audioSide.selectElement('album__item_recomendations');
            this.songsCollection.fetch({
                reset: true,
                action: 'audio.getRecommendations',
                data: {
                    shuffle: 0
                }
            });
        }
	});

	return SiteController;
})