define(function(require) {
    var _ = require('underscore'),
        Backbone = require('backbone'),
        DeviceModel = require('models/device');

    var DevicesCollection = Backbone.Collection.extend({
        url: '/api/device',
        model: DeviceModel,
        currentDevice: new DeviceModel,
        initialize: function() {
            this.on('reset', function() {
                var device = this.getCurrentDevice();
                var model = this.find(function(ml) {
                    return ml.getId() == device.getId();
                });

                if(!_.isUndefined(model)) {
                    this.remove(model);
                }
            }, this);
        },
        getActive: function() {
            return this.filter(function(model) {
                return model.get('isActive');
            });
        },
        getOneActive: function() {
            var models = this.getActive();

            if(0 < models.length) {
                return models[0];
            } else {
                return null;
            }
        },
        getCurrentDevice: function() {
            return this.currentDevice;
        },
        setCurrentDevice: function(device) {
            this.currentDevice = device;
        }
    });

    return DevicesCollection;
});