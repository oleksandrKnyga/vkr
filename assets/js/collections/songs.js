define(function(require) {
	var Backbone = require('backbone'),
		SongModel = require('models/song'),
        vkapi = require('vkapi');

	var SongsCollection = Backbone.Collection.extend({
		url: '/api/song',
		model: SongModel,
		isFetched: false,
		activePosition: -1,
		isPlaying: false,
        lastPlayModel: null,
        token: '',
		getPosition: function(dest) {
			var pos = -1;

			this.each(function(model, i) {
				if(model.get(model.idAttribute) == dest.get(dest.idAttribute)) {
					pos = i;
				}
			});

			return pos;
		},
		getActivePosition: function() {
			if(0 > this.activePosition) {
                return 0;
            } else {
                return this.activePosition;
            }
		},
		setActivePosition: function(pos, isNotify) {
            if(_.isUndefined(isNotify)) {
                isNotify = true;
            }

			var oldPos = this.activePosition;
			this.activePosition = pos;

			if(oldPos != pos && isNotify) {
				this.trigger('change:position', pos);
			}
		},
		getActiveModel: function() {
			return this.at(this.getActivePosition());
		},
		setActiveModel: function(sm) {
			var pos = -1;
			this.each(function(model, i) {
				if(model.get(model.idAttribute) == sm.get(model.idAttribute)) {
					pos = i;
				}
			});
			this.setActivePosition(pos);
		},
		getNextPosition: function() {
			var pos = this.getActivePosition();

			if(pos < this.length - 1) {
				return pos+1;
			} else {
				return 0;
			}
		},
		getNextModel: function() {
            var nModel = this.at(this.getNextPosition()),
                currentModel = this.getActiveModel(),
                lm = this.lastPlayModel;

            if(_.isNull(lm) || lm.getId() != currentModel.getId()) {
                nModel = currentModel;
            }

			return nModel;
		},
		getPrevPosition: function() {
			var pos = this.getActivePosition();

			if(pos > 0) {
				return pos - 1;
			} else {
				return this.length - 1;
			}
		},
		getPrevModel: function() {
			return this.at(this.getPrevPosition());
		},
		pause: function() {
			this.isPlaying = false;
			this.getActiveModel().pause();
			//this.trigger('pause');
		},
		play: function() {
			this.isPlaying = true;
            var activeModel = this.getActiveModel();
			activeModel.play();
            this.lastPlayModel = activeModel;
			//this.trigger('play');
		},
		toggle: function() {
            var lm = this.lastPlayModel;

            if(this.isPlaying && (_.isNull(lm) || lm.getId() != this.getActiveModel().getId())) {
                this.pause();
            }

			if(this.isPlaying) {
				this.pause();
			} else {
				this.play();
			}
		},
		getIsPlaying: function() {
			return this.isPlaying;
		},
		toggleModel: function(model) {
			var oldPosition = this.getActivePosition(),
				oldModel = this.getActiveModel();
			this.setActiveModel(model);
			var newPosition = this.getActivePosition();

			if(oldPosition != newPosition && oldModel) {
				oldModel.pause();
			}

			if(oldPosition != newPosition && this.getIsPlaying()) {
				this.play();
			} else {
				this.toggle();
			}
		},
        fetch: function(options) {

            options = _.extend({
                reset: false,
                action: 'audio.get',
                data: {},
                callback: function(response) {
                    if(_.isUndefined(response.response.items)) {
                        this.set(response.response);
                    } else {
                        this.set(response.response.items);
                    }

                    if(options.reset) {
                        this.trigger('reset', this, options);
                    }
                }
            }, options);

            vkapi.exec(options, this);
        },
        getToken: function() {
            return this.token;
        },
        setToken: function(token) {
            this.token = token;
        }
	});

	return SongsCollection;
})