/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs				:: http://sailsjs.org/#!documentation/models
 */

var noop = function() {};

module.exports = self = {
	autoCreatedAt: false,
	autoUpdatedAt: false,
	attributes: {
		uid: {
			type: 'integer',
			primaryKey: true
		},
		first_name: 'string',
		last_name: 'string',
		sex: 'integer',
		nickname: {
			type: 'string',
			unique: true
		},
		screen_name: {
			type: 'string',
			unique: true
		},
		bdate: 'integer',
		city: 'integer',
		country: 'integer',
		timezone: 'integer',
		photo: 'string',
		photo_medium: 'string',
		photo_big: 'string',
		has_mobile: 'BOOLEAN',
		online: 'BOOLEAN',
		home_phone: 'string',
		counters: 'JSON',
		university: 'integer',
		university_name: 'string',
		faculty: 'integer',
		faculty_name: 'string',
		graduation: 'integer',
		device_primary: 'integer',
		lang: 'string',
		created_time: 'integer',
		updated_time: 'integer',
		active_playlist_rule: {
			type: 'json'
		},
		/*device_primary: {
			model: 'device'
		},*/
		getId: function() {
			return this.id;
		},
		getPlaylist: function(cb) {
			var that = this;
			var vk = sails.config.globals.vk,
				requestName = 'audio.' + this.active_playlist_rule.method;

			vk.request(requestName, this.active_playlist_rule.params);
			vk.on('done:' + requestName, function(playlist) {
				cb.call(that, playlist.response);
			});
		},
		fetchProfile: function(callback) {
			User.fetchProfile(this.uid, callback);
		},
		updateProfile: function(callback) {
			if ("function" !== typeof callback) {
				callback = function() {};
			}

			var that = this;
			this.fetchProfile(function(udata) {
				//todo: update only fields which changed
				User.update({
					uid: that.uid
				}, udata).exec(function(err, users) {
					if (users && users.length > 0) {
						arguments[1] = users[0];
						callback.apply(this, arguments);
					} else {
						callback.apply(this, arguments);
					}
				});
			});
		},
		update: function(arr, cb) {
			if(!_.isArray(arr)) {
				arr = [arr];
			}

			var so = {};

			for(var i=0;i<arr.length;i++) {
				so[arr[i]] = this[arr[i]];
			}

			User.update({
				uid: this.uid
			}, so, cb);
		},
		checkPrimaryDevice: function(deviceId, cb) {
			var that = this;

			if(!_.isFunction(cb)) {
				cb = noop;
			}

			if(this.device_primary) {

				Device.findOne({
					id: deviceId
				}, function(err, device) {

					if(err || !device) {
						cb.apply(this, arguments);
					} else {

						if(!device.online && device.device_type == 'desktop') {
							that.device_primary = deviceId;
							that.save(cb);
						} else {
							cb.apply(this, arguments);
						}
					}
				});

			} else {
				that.device_primary = deviceId;
				that.save(cb);
			}
		}
	},
	fetchProfile: function(uid, callback) {
		var vk = sails.config.globals.vk;
		vk.request('getProfiles', {
			'uids': uid,
			fields: ['id',
				'first_name', 'last_name', 'nickname', 'screen_name',
				'sex', 'bdate', 'city', 'country', 'timezone',
				'photo', 'photo_medium', 'photo_big', 'has_mobile',
				'rate', 'contacts', 'education', 'online',
				'counters'
			].join(',')
		});
		vk.on('done:getProfiles', function(vkresp) {
			var udata = vkresp.response[0];

			if ("function" === typeof callback) {
				callback(udata);
			}
		});
	},
	beforeUpdate: function(values, next) {
		values.updated_time = Math.floor(new Date().getTime() / 1000);
		next();
	},
	beforeCreate: function(values, next) {
		values.created_time = Math.floor(new Date().getTime() / 1000);
		values.active_playlist_rule = {
			method: 'get',
			params: {},
			aid: 0
		};
		next();
	},
	init: function(attributes) {
		var md = {};

		for(var name in self.attributes) {

			if(_.isFunction(self.attributes[name])) {
				md[name] = self.attributes[name];
			} else {
				md[name] = null;
			}
		}

		return _.extend({}, md, attributes);
	}
};