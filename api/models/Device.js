/**
* Device.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs				:: http://sailsjs.org/#!documentation/models
*/

var crypto = require('crypto');
module.exports = Device = {
	autoCreatedAt: false,
	autoUpdatedAt: false,
	attributes: {
		id: {
			type: 'integer',
			primaryKey: true
		},
		hash: {
			type: 'string',
			unique: true
		},
		os_name: 'string',
		os_version: 'string',
		browser_name: 'string',
		browser_version: 'string',
		device_type: 'string',
		device_manufacturer: 'string',
		device_model: 'string',
		user_uid: 'integer',
		ip: 'string',
		created_time: 'integer',
		updated_time: 'integer'
	},
	getHash: function(data) {
		var fdata = {},
			allowed = ['os_name', 'os_version', 'browser_name', 
			'browser_version', 'device_type', 'device_manufacturer', 
			'device_model', 'user_uid', 'ip'];

		for(var name in data) {

			if(allowed.indexOf(name) > -1) {
				fdata[name] = data[name];
			}
		}

	  	var hash = crypto.createHash('md5').update(JSON.stringify(fdata)).digest('hex');
	  	return hash;
	},
	getParamsFromWhichBrowser: function(data) {
		var params = {};

		if(data.os && data.os.name) {
			params.os_name = data.os.name;
		}

		if(data.os && data.os.version) {
			params.os_version = data.os.version.alias ? data.os.version.alias : data.os.version.value;
		}

		if(data.browser && data.browser.name) {
			params.browser_name = data.browser.name;
		}

		if(data.browser && data.browser.version) {
			params.browser_version = data.browser.version.alias ? data.browser.version.alias : data.browser.version.value;
		}

		if(data.device && data.device.type) {
			params.device_type = data.device.type;
		}

		if(data.device && data.device.manufacturer) {
			params.device_manufacturer = data.device.manufacturer;
		}

		if(data.device && data.device.model) {
			params.device_model = data.device.model;
		}

		return params;
	},
	beforeUpdate: function(values, next) {
		values.updated_time = Math.floor(new Date().getTime()/1000);
		next();
	},
	beforeCreate: function(values, next) {
		values.created_time = Math.floor(new Date().getTime()/1000);
		next();
	},
	init: function(attributes) {
		var md = {};

		for(var name in self.attributes) {

			if(_.isFunction(self.attributes[name])) {
				md[name] = self.attributes[name];
			} else {
				md[name] = null;
			}
		}

		return _.extend({}, md, attributes);
	}
};

