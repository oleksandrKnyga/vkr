/**
 * DeviceController
 *
 * @description :: Server-side logic for managing devices
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	
	play: function(req, res) {
		var socket = req.socket;
		var io = sails.io;
		 
		// emit to all sockets (aka publish)
		// including yourself
		//io.sockets.emit('message', {thisIs: 'theMessage'});

		// broadcast to a room (aka publish)
		// excluding yourself, if you're in it
		//socket.broadcast.to('dev2').emit('message', {thisIs: 'theMessage'});
	}
};

