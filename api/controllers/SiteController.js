/**
 * SiteController
 *
 * @description :: Server-side logic for managing sites
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {

  logout: function(req, res) {
    res.clearCookie('user');
    res.redirect('/');
  },

  /**
   * `SiteController.index()`
   */
  index: function (req, res) {
      if(req.cookies.user) {
          var ip = req.headers['X-Real-IP'] || req.connection.remoteAddress;
          res.cookie('ip', ip);
          res.view();
      } else {
          var cf = sails.config.vk;
          var loginUrl = 'http://' + cf.mode +
              '.vk.com/authorize?client_id=' + cf.appID +
              '&redirect_uri=' + encodeURIComponent(cf.redirectUri) +
              '&response_type=token' +
              '&display=popup' +
              '&v=5.27' +
              '&scope=' + cf.scope;

          res.render('site/home-guest', {
              loginUrl: loginUrl
          });
      }
  },

  vkauth: function(req, res) {
      res.render('site/vkauth');
  }
};

