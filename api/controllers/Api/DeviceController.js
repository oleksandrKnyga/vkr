/**
 * Api/DeviceController
 *
 * @description :: Server-side logic for managing api/songs
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	index: function(req, res) {
        var data = sails.config.configuration.usersData,
            id = req.query.user_id;

        if(data.hasOwnProperty(id)) {
            var sendData = [];
            _.each(data[id], function(name, id) {
                console.log(name, id);
                sendData.push({
                    id: id,
                    name: name
                });
            });

            res.send(sendData);
        } else {
            res.send([]);
        }

//        res.send([{
//            id: 1,
//            name: 'Windows 7, Chrome'
//        },{
//            id: 2,
//            name: 'Ubuntu 12.1, Firefox'
//        },{
//            id: 3,
//            name: 'iOs 10, Chrome'
//        }]);
  }
};

