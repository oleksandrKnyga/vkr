/**
 * Api/songController
 *
 * @description :: Server-side logic for managing api/songs
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	index: function(req, res) {
        var type = req.query.type || false,
            user = User.init(req.session.user),
            newMethod = '',
            newParams = {};

        switch(type) {
            case 'search':
                newMethod = 'search';
                newParams = {
                    q: req.query.q
                };
            break;
            default:
            case 'list':
                newMethod = 'get';
                newParams = {};
             break;
        }

        user.active_playlist_rule = {
            method: newMethod,
            params: newParams
        };
        User.update({
            uid: user.uid
        }, {
            active_playlist_rule: user.active_playlist_rule
        }).exec(function(err) {
            if(err) {
                console.log(err);
            }
        });

        user.getPlaylist(function(playlist) {
            try {
                res.send(playlist);
            } catch(e) {
                console.log(e);
            }
        });
  }
};

