#!/bin/sh

if [ "$1" = "" ] || [ "$1" = "--css" ]
then
	echo "--css"
	"node_modules/.bin/borschik" --input=assets/css/main.css --output=assets/css/main.built.temp1.css --minimize=false --comments=false
	echo "built"
	sed "s/﻿﻿//g" assets/css/main.built.temp1.css > assets/css/main.built.temp2.css
	sed "s/﻿//g" assets/css/main.built.temp2.css > assets/css/main.built.temp3.css
	"node_modules/.bin/uglifycss" --cute-comments assets/css/main.built.temp3.css > assets/css/main.built.css
	echo "optimised"
	rm assets/css/main.built.temp1.css
	rm assets/css/main.built.temp2.css
	rm assets/css/main.built.temp3.css
	echo "temp files removed"
fi

if [ "$1" = "" ] || [ "$1" = "--js" ]
then
	echo "--js"
	node_modules/.bin/r.js -o assets/js/build.js
fi

#